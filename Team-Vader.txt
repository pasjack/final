You just traveled the galaxy to meet your father, you refuse to join
Palpatine and take the side of your father. Darth Vader can not
believe that the Emperor has betrayed him after all of these years
and takes your side.

Palpatine summons hundreds of Storm Troopers and orders them to kill you
and Vader. They begin open firing on you. You and Vader deflect hundreds
of shots back at the troopers with your light sabers which takes out tons
of them. Suddenly, the few remaining troopers stop firing.

You turn and see that the Emperor holding Vaders helmet in his hand,
Vaders lifeless body lies on the floor nearby. Palpatine chuckles and
summons someone new to the throne room.

Your brother Luke Skywalker walks in. Palpatine exclaims that Luke is
the one he wanted and he only brought you here so he could kill you
and make sure you never become a Jedi. A Storm Trooper shoots you in the
leg, Luke approaches and draws his light saber. The last thing you see
is a red flash coming toward your neck.

The End.
