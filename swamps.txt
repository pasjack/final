You have chosen to bait Darth Vader and Emperor Palpatine onto your home planet before they blow
it up. You send out a message through the Force, taunting them to come fight you. You have little 
time to prepare before the arrive, and as they approach you know the final battle is coming. 

Darth Vader attempts one last time to sway you to his side. He tells you to turn your back on the
ways of the Jedi, to join with him against Palpatine, and from there rule the world. You agree, and
you both turn on the Emperor. 
"I expected this" Palpatine says, and he reaches out with his dark powers, taking control of R3-D3.
The four of you go to battle, exchanging lightsaber blows, lighting, and blaster shots. After 
Palpatine knocks you over with lightning, R3-D3 charges a laser in your direction. Before he can 
fire, Vader cuts him and half! You mourn the loss of your friend, but do not have time. As the two
of you turn on Palpatine, you can feel yourself and Vader tiring out. Palpatine chooses this
moment to strike, pulling out a red lightsaber, twisting through the air at your father. But you
have the high ground, and as Palpatine leaps at Vader, you jump in and cut Palpatine in half.

But you haven't forgot what Vader did to your siblings. "For Luke and Leia" you say as you rip off
his helmet and cut off his head. You run over to the body of R3-D3, and take his data card out of
his corpse. You take the data card, and get to work uploading it to Vader's suit.

The End 