You receive a hologram from Darth Vader, he is so pleased that at least one of
his children have joined the Dark Side. He sends an Imperial TIE fighter to bring
you to the Death Star where you will meet Emperor Palpatine and your father
face to face.

You arrive to the Death Star and are greeted by an army of Storm Troopers, all of
which bow down upon your arrival. An Imperial General approaches, he is carrying
a black robe and a red light saber. He tells you that Lord Palpatine insists you get rid
of the brown robe and green light saber that Yoda had gifted you before you left
Dagobah. He then escorts you to your new living quarters on the Death Star.

After you settle in, you are summoned to the throne room to finally meet the Emperor.
The Emperor explains how happy he is to have another Skywalker joining the Dark Side.
He has not trained anyone in the dark side of the force since Darth Maul moons ago.

His first question, do you want to learn the Force choke, or the force lightning?

Enter "choke" to learn the force choke

Enter "lightning" to learn force lightning
