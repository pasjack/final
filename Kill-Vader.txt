You have traveled the galaxy to become a Sith Lord, you must fulfill
your destiny. Even though he is your father, you barely know him, Yoda
is the one who raised you. You summon the force and raise Darth Vader
into the air.

The thought of Yoda makes you rethink your decision, is this what Yoda
would do? You drop Vader to the ground and exclaim to Palpatine that
you will not kill your father. He chuckles and tells you that he knew
you could never be a true Sith Lord.

He draws his light saber and approaches you. You draw yours and engage
Palpatine. He is much more experienced than you and he is dominating you,
he makes a swift move and cuts off your leg.

He chuckles and uses the force as he intends to finish you off with the
force choke. You feel your lungs getting tight and you begin to loose
consciousness. At the last second, Vader draws his light saber and cuts
off Palpatines head. He orders the droids to build you a new leg and you
retreat to Dagobah to rejoin Yoda in Jedi training with your brother Luke.

The End 
