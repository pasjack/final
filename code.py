import sys #allows for system.exit
from turtle import * #Allows for turtle to run in program
import time #allows us to us the sleep function for the lightsaber image
i = 0 #variable used for lightsaber color and turtle animation.

# Function that writes a line from the file
# Param: (line) string containing a line from the file
def writes(line):
    write(line)
    penup()
    right(90)
    forward(30)
    left(90)
    pendown()

# Function that draws a green or red lightsaber based which side the user chooses
# Param: (sabercolor) string containing side choice jedi/sith
def lightsaber(sabercolor):

  # move pen to center and change color
  penup()
  clear()
  goto(0,-150)
  pendown()
  if(sabercolor == 'sith'):
    color("red")
  else:
    color("green")

  # Draw blade
  begin_fill()
  left(90)
  forward(300)
  circle(20, 180)
  forward(300)
  left(90)
  forward(40)
  end_fill()

  # draw handle
  begin_fill()
  color("grey")
  right(90)
  forward(100)
  right(90)
  forward(40)
  right(90)
  forward(100)
  right(90)
  forward(40)
  end_fill()

# Function that moves turtle to start and reads in the file
# Param: (text) string that contains the file name
def start_file(text):
  penup()
  goto(-500, 250)
  clear()
  pendown()
  readfile(text)
  hideturtle()

# function that changes bacgkround color to black, and pen color to yellow
def background():
    bgcolor("black")
    color("yellow")


# Function that reads and writes from a text file line by line
# Param: (text) string containing file name
def readfile(text):
  with open(text) as f:
      lines = f.readlines() # list containing all the lines in the file
  for line in lines: # loop through list and calls the write for each line
    writes(line)

    

def text_loop(i):
  # Gets input and gives option to quit
    text = textinput("Choice","What choice do you want to make?")
    if text == "quit":
        sys.exit()
    if (i == 0): # draws lightsaber after first choice user makes
        lightsaber(text)
        time.sleep(5)
        color("yellow")
    i += 1
    text = text + ".txt"
    start_file(text)







# run_program combines all of the previous functions, so when run_program is called it will call all other functions in descending order
def run_program():
    text = 'intro.txt' # Initilize text as the name of the first file
    background()
    start_file(text) # Writes the first text file
    for i in range(3):
        text_loop(i)

run_program()
done()
